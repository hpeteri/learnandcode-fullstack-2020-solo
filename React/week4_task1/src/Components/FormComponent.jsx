import React, {useState} from "react";
import axios from "axios";


const FormComponent = () => {
    const [endpoint, SetEndpoint] = useState("");   
    const [body, SetBody] = useState("");
    
    const HandleEndpointChange = (e) => {
        SetEndpoint(e.target.value);
    };
    const HandleBodyChange = (e) => {
        SetBody(e.target.value);
    };
    const HandleFormSubmit = async (e) => {
        e.preventDefault()
        console.log(body);
        try {
            const result = await axios.post(`https://jsonplaceholder.typicode.com/${endpoint}`,
                                            JSON.parse(body));
            console.log(result.data);  
        } catch(error) {
            console.log(error);
        } 
    };
    return (
        <div>
          Post
          <form method="post" onSubmit={(e) => HandleFormSubmit(e)}>
            Endpoint
            <br/>
            <input onChange={(e) => HandleEndpointChange(e)}/>
              
              
              <br/>
              Data
              <br/>
              <input onChange={(e) => HandleBodyChange(e)}/>
                <br/>
                <button type="submit"> Post </button>
          </form>
        </div>
    );
};
export default FormComponent;
