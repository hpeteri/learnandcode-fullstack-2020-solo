import React , {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';

const App = () => {
    const [time, setTime] = useState(new Date().toLocaleString());
    const [mountTime] = useState(new Date().toLocaleString());
    const [paused, setPaused] = useState(false);
    useEffect(() => { //use effect is called after DOM is rendered
        if(paused) return;
        
        
        const interval = setInterval(() => {
            setTime(new Date().toLocaleString());    
        }, 1000);
        return () => {
            //this is called when cleanup is called, so when we update the dom which was modified
            clearInterval(interval);
        }

    }, []); //we specify if we want to loop
    return (
            <div>
            {mountTime}
            <br/>
            <button onClick={(e)=>{setPaused(!paused)}}> {(paused && "paused") || "On"}</button>
       
            <br/>
        {time}
        </div>
    );
};
           //HELLO :: Time is {time}
ReactDOM.render(<App />, document.getElementById("root"));
