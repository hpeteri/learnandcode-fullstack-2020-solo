import {
    DoAccountsExist,
    GetAccountWithID,
} from "./programState.js";
import { GetCurrentAccountOrLogIn } from "./commandsAccount.js";
import {
    GetUserInput,
    OutputString,
    GetValidAccountIDFromUser,
} from "./dialog.js";

function WithdrawFunds() {
    if (!DoAccountsExist()) return;
    OutputString("Okay, let's whip up some cash for you from these ones and zeroes.");

    const account = GetCurrentAccountOrLogIn();

    OutputString("How much money do you want to withdraw? " +
                 `(Current balance: ${account.balance}€)`);
    const run = true;
    while (run) {
        const n = parseInt(GetUserInput(), 10);
        if (Number.isNaN(n) || n <= 0) {
            OutputString("Enter a valid number!");
        } else if (n > account.balance) {
            OutputString("Unfortunately you don't have the balance for that. " +
                         "Let's try a smaller amount.");
        } else {
            account.balance -= n;
            OutputString(`Awesome, you can now enjoy your ${n}€ in cash! ` +
                         `There's still ${account.balance}€ in your account, ` +
                         "safe with us.");
            break;
        }
    }
}
function DepositFunds() {
    if (!DoAccountsExist()) return;
    OutputString("Okay, let's convert your cash in to some delicious ones and zeroes, " +
                 "then feed them in to your hungry system.");

    const account = GetCurrentAccountOrLogIn();

    OutputString("How much money do you want to deposit? " +
                 `(Current balance: ${account.balance}€)`);
    const run = true;
    while (run) {
        const n = parseInt(GetUserInput(), 10);
        if (Number.isNaN(n) || n <= 0) {
            OutputString("Enter a valid number!");
        } else {
            account.balance += n;
            OutputString(`Awesome, we removed ${n}€ from existance ` +
                         "and stored them in to our system. " +
                         `Now your accounts balance is ${account.balance}€.`);
            break;
        }
    }
}
function TransferFunds() {
    if (!DoAccountsExist()) return;
    OutputString("Okay, let's slide these binary treats into someone elses pockets.");

    const account = GetCurrentAccountOrLogIn();

    OutputString("How much money do you wish to transfer? " +
                 `(Current balance: ${account.balance}€)`);
    const run = true;
    while (run) {
        const n = parseInt(GetUserInput(), 10);
        if (Number.isNaN(n) || n <= 0) {
            OutputString("Enter a valid number!");
        } else if (n > account.balance) {
            OutputString("Unfortunately you don't have the balance for that. " +
                         "Let's try a smaller amount.");
        } else {
            OutputString("Awesome, What is the ID of the account you want to tranfer " +
                         "these funds to? ");

            const destID = GetValidAccountIDFromUser(true);
            const destAccount = GetAccountWithID(destID);
            if (destAccount === account) {
                OutputString("Hmm, That's seems to be your account.");
                return;
            }
            account.balance -= n;
            destAccount.balance += n;
            OutputString(`Awesome, we sent ${n}€ to an account with the id of ${destID}.`);
            break;
        }
    }
}

export {
    WithdrawFunds,
    DepositFunds,
    TransferFunds,
};
