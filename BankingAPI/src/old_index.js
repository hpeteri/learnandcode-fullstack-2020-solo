import express from "express";
import bodyParser from "body-parser";
import fs from "fs";

const app = express();

app.use(bodyParser.json());

const accountFileName = "accounts.json";

function LoadAccounts() {
    let allAccounts = [];
    try {
        const data = fs.readFileSync(accountFileName, { encoding: "utf8", flags: "r" });
        allAccounts = JSON.parse(data);
    } catch (e) {
        console.error("!!! Failed to open account file !!!");
        console.log(e);
    }
    return allAccounts;
}
function SaveAccounts(accounts) {
    console.log(accounts);
    try {
        const serializedAccounts = JSON.stringify(accounts);
        fs.writeFileSync(accountFileName, serializedAccounts);
    } catch (e) {
        console.error("!!! Failed to save account file !!!");
        console.log(e);
    }
}

// returns created account id
app.post("/bank/user", (req, res) => {
    const accounts = LoadAccounts();

    const id = accounts.reduce((max, it) => Math.max(max, it.id), 0);

    const newAccount = { ...req.body, id: id + 1 };
    newAccount.balance = parseInt(newAccount.balance, 10);
    newAccount.balance = (Number.isNaN(newAccount.balance)) ? 0 : newAccount.balance;
    SaveAccounts([...accounts, newAccount]);

    res.json(newAccount.id);
});

function FindAndVerifyAccount(accounts, id, password, ignorePassword) {
    if (accounts !== undefined && !Number.isNaN(id)) {
        const account = accounts.find((it) => (it.id === id));
        if (account && (ignorePassword || account.password === password)) {
            return account;
        }
    }

    return undefined;
}

// GetAccountBalance
// returns accounts balance if accounts is found and password is correct
// otherwise set status to 404
app.get("/bank/:user_id/balance/", (req, res) => {
    const allAccounts = LoadAccounts();
    const account = FindAndVerifyAccount(allAccounts,
        parseInt(req.params.user_id, 10),
        req.body.password);
    if (account) {
        res.json(account.balance);
    } else {
        console.log("Not Found or password was incorrect");
        res.status(404).end();
    }
});
// Withdraw
// returns new balance
// returns -1 if request was denied
app.put("/bank/:user_id/withdraw/", (req, res) => {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount < 0) {
        res.json({ error: "-1" });
        return;
    }
    const allAccounts = LoadAccounts();
    const account = FindAndVerifyAccount(allAccounts,
        parseInt(req.params.user_id, 10),
        req.body.password);
    if (account && account.balance >= amount) {
        account.balance -= amount;
        SaveAccounts(allAccounts);

        res.json(account.balance);
        return;
    }
    res.json({ error: "-1" });
});
// Deposit
// returns new balance
// returns -1 if request was denied
app.put("/bank/:user_id/deposit/", (req, res) => {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount)) {
        res.json({ error: "-1" });
        return;
    }
    const allAccounts = LoadAccounts();
    const account = FindAndVerifyAccount(allAccounts,
        parseInt(req.params.user_id, 10),
        req.body.password);
    if (account) {
        account.balance += amount;
        SaveAccounts(allAccounts);

        res.json(account.balance);
        return;
    }
    res.json({ error: "-1" });
});
app.put("/bank/:user_id/transfer/", (req, res) => {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount < 0) {
        res.json({ error: "-1" });
        return;
    }
    const allAccounts = LoadAccounts();
    const recipient = FindAndVerifyAccount(allAccounts,
        parseInt(req.body.recipient_id, 10),
        undefined,
        true);

    const account = FindAndVerifyAccount(allAccounts,
        parseInt(req.params.user_id, 10),
        req.body.password);

    if (account && recipient && account.balance >= amount) {
        account.balance -= amount;
        recipient.balance += amount;

        SaveAccounts(allAccounts);
        res.json(account.balance);
        return;
    }
    res.json({ error: "-1" });
});

app.put("/bank/:user_id/name", (req, res) => {
    const allAccounts = LoadAccounts();
    const account = FindAndVerifyAccount(allAccounts,
        parseInt(req.params.user_id, 10),
        req.body.password);
    if (account) {
        account.name = req.body.newName;
        SaveAccounts(allAccounts);
        res.json(account.name);
        return;
    }
    res.json({ error: "-1" });
});
app.put("/bank/:user_id/password", (req, res) => {
    const allAccounts = LoadAccounts();
    const account = FindAndVerifyAccount(allAccounts,
        parseInt(req.params.user_id, 10),
        req.body.password);
    if (account) {
        account.password = req.body.newPassword;
        SaveAccounts(allAccounts);
        res.json(account.password);
        return;
    }
    res.json({ error: "-1" });
});

app.listen(5000, () => console.log("Listening to port 5000"));
