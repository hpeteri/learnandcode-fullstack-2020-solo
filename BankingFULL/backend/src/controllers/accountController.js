import bcrypt from "bcrypt";
import AccountModel from "../models/accountModel.js";
import HTTPStatusCode from "../httpStatusCodes.js";
import { CreateTokens } from "../authentication.js";

// @NOTE
// We authenticate user request in middleware (../authentication.js)
// which includes the validated account in req.body.account
// if account is not found, then middleware returns early and request doesn't
// get here, so checking if account is undefined is pointless.

async function GenerateValidID() {
    const newID = Math.floor((Math.random() * 1000000));
    const found = await AccountModel.findOne({ id: newID }).exec();
    // Would rather do this in a loop but eslint doesn't like await in loops...
    if (!found) return newID;
    return GenerateValidID();
}
export async function Login(req, res) {
    const {
        userID,
    } = req.body;
    const tokens = userID ? CreateTokens(userID) : { token: null, refresthToken: null };
    res.cookie("refreshToken", tokens.refreshToken)
        .status(200)
        .json({ token: tokens.token });
}
export async function Logout(req, res) {
    res.clearCookie("refreshToken")
        .status(200)
        .json({ token: null });
}
export async function CreateAccountOrFail(req, res) {
    const { name, balance } = req.body;
    const password = bcrypt.hashSync(req.body.password, 10);
    const account = new AccountModel({
        name,
        password,
        balance,
        id: await GenerateValidID(),
    });
    await account.save();
    const tokens = CreateTokens(account.id);
    res.cookie("refreshToken", tokens.refreshToken).status(HTTPStatusCode.OK).json({ token: tokens.token });
}
export async function GetAccountBalanceOrFail(req, res) {
    res.status(HTTPStatusCode.OK).json(req.body.account.balance);
}
export async function WithdrawFromAccountOrFail(req, res) {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount <= 0) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    const { account } = req.body;
    if (account.balance >= amount) {
        const updatedAccount = await AccountModel.findOneAndUpdate(
            { id: account.id },
            { balance: account.balance - amount },
            { useFindAndModify: false, new: true },
        );
        res.status(HTTPStatusCode.OK).json(updatedAccount.balance);
    } else {
        res.status(HTTPStatusCode.Not_Found).json(undefined);
    }
}
export async function DepositToAccountOrFail(req, res) {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount <= 0) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    const { account } = req.body;
    const updatedAccount = await AccountModel.findOneAndUpdate(
        { id: account.id },
        { balance: account.balance + amount },
        { useFindAndModify: false, new: true },
    );
    res.status(HTTPStatusCode.OK).json(updatedAccount.balance);
}
export async function TransferToAccountOrFail(req, res) {
    console.log("TRANSFER");

    const { amount } = req.body;
    console.log(req.body);
    if (Number.isNaN(amount) || amount <= 0) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    const recipientFilter = {
        id: req.body.recipientID,
    };
    const { account } = req.body;
    const recipient = await AccountModel.findOne(recipientFilter).exec();
    console.log(account);
    console.log(recipient);
    if (account.balance >= amount && recipient) {
        const updatedAccount = await AccountModel.findOneAndUpdate(
            { id: account.id },
            { balance: account.balance - amount },
            { useFindAndModify: false, new: true },
        );
        await AccountModel.findOneAndUpdate(
            { id: recipient.id },
            { balance: recipient.balance + amount },
            { useFindAndModify: false, new: true },
        );

        res.status(HTTPStatusCode.OK).json(updatedAccount.balance);
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
export async function ChangeAccountHolderInfoOrFail(req, res) {
    const { account } = req.body;
    const newData = {
        name: (req.body.newName) ? req.body.newName : account.name,
        password: (req.body.newPassword) ? bcrypt.hashSync(req.body.newPassword, 10) : account.password,
    };
    await AccountModel.findOneAndUpdate(
        { id: account.id },
        newData,
        { useFindAndModify: false, new: true },
    );
    res.status(HTTPStatusCode.OK).json(newData);
}
export async function DeleteAccountOrFail(req, res) {
    const account = await AccountModel.findOneAndRemove({ id: req.body.account.id },
        {
            useFindAndModify: false,
            new: true,
        }).exec();
    res.status(HTTPStatusCode.OK).json(account);
}
