import AccountModel from "../models/accountModel.js";
import FundRequestModel from "../models/fundRequestModel.js";
import HTTPStatusCode from "../httpStatusCodes.js";

// @NOTE
// We authenticate user request in middleware (../authentication.js)
// which includes the validated account in req.body.account
// if account is not found, then middleware returns early and request doesn't
// get here, so checking if account is undefined is pointless.

async function GenerateValidRequestID() {
    const newID = Math.floor((Math.random() * 1000000));
    const found = await FundRequestModel.findOne({ request_id: newID }).exec();
    // Would rather do this in a loop but eslint doesn't like await in loops...
    if (!found) return newID;
    return GenerateValidRequestID();
}

export async function RequestFundsOrFail(req, res) {

    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) ||
        amount <= 0 ||
        req.body.responder === req.body.account.id) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    console.log(req.body);
    const responderFilter = {
        id: req.body.responder,
    };
    const issuer = req.body.account;
    const responder = await AccountModel.findOne(responderFilter).exec();

    if (responder) {
        const request = new FundRequestModel({
            request_id: await GenerateValidRequestID(),
            issuer_id: issuer.id,
            responder_id: responder.id,
            request_amount: amount,
        });
        console.log(request);
        await request.save();
        res.status(HTTPStatusCode.OK).send(request);
    } else {
        res.status(HTTPStatusCode.Not_Found).end();
    }
}
export async function GetFundRequestsOrFail(req, res) {
    const { account } = req.body;

    const filter = { responder_id: account.id };
    const requests = await FundRequestModel.find(filter).exec();
    if (requests) {
        // We should only get request that are from or for this user
        const filteredRequests = requests.filter((it) => (it.issuer_id === account.id ||
                                                          it.responder_id === account.id));
        res.status(HTTPStatusCode.OK).send(filteredRequests);
    } else {
        console.log("This should never happen FIND() returns null");
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
export async function AcceptFundRequestOrFail(req, res) {
    console.log("Accept ???");
    console.log(req.body);
    const requestID = parseInt(req.body.request_id, 10);
    if (Number.isNaN(requestID)) {
        res.status(HTTPStatusCode.Bad_Request).end();
        return;
    }
    const { account } = req.body;
    const request = await FundRequestModel.findOne({
        request_id: requestID,
        responder_id: account.id,
    }).exec();
    if (request) {
        const issuer = await AccountModel.findOne({ id: request.issuer_id }).exec();
        if (issuer && account.balance >= request.request_amount) {
            account.balance -= request.request_amount;
            issuer.balance += request.request_amount;

            await FundRequestModel.findOneAndRemove(
                { request_id: request.request_id },
                { useFindAndModify: false },
            ).exec();

            await AccountModel.findOneAndUpdate(
                { id: account.id },
                { balance: account.balance },
                { useFindAndModify: false, new: true },
            ).exec();

            await AccountModel.findOneAndUpdate(
                { id: issuer.id },
                { balance: issuer.balance },
                { useFindAndModify: false },
            ).exec();

            res.status(HTTPStatusCode.OK).json({ newBalance: account.balance });
        } else {
            res.status(HTTPStatusCode.Not_Found).send(undefined);
        }
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
export async function DeclineFundRequestOrFail(req, res) {
    console.log("???");
    const requestID = parseInt(req.body.request_id, 10);
    if (Number.isNaN(requestID)) {
        res.status(HTTPStatusCode.Bad_Request).end();
        return;
    }

    const { account } = req.body;

    const request = await FundRequestModel.findOne({
        request_id: requestID,
        responder_id: account.id,
    }).exec();
    if (request) {
        const ret = await FundRequestModel.findOneAndRemove(
            { request_id: request.request_id },
            { useFindAndModify: false },
        ).exec();

        res.status(HTTPStatusCode.OK).json(ret);
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
