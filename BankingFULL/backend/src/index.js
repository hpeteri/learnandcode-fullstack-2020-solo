import express from "express";
import cookieParser from "cookie-parser";
import accountRouter from "./routers/accountRouter.js";
import ConnectToDB from "./DB.js";

const app = express();

app.use(cookieParser());
app.use(express.json());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type, Accept, Authentication");
    next();
});
/// ///////////////////////////////////////////////////////////////
// Debug
/*
app.use((req, res, next) => {
    if (req.method !== "OPTIONS") {
        console.log("\nMiddleware");
        const {
            cookies,
        } = req;
        console.log(req.method);
        console.log(req.path);
        console.log(req.body);
        console.log(req.query);
        console.log(cookies);
        console.log("\n");
    }
    next();
});
*/
/// ///////////////////////////////////////////////////////////////

app.use("/bank/", accountRouter);

if (ConnectToDB()) {
    app.listen(5000, () => console.log("Listening to port 5000"));
}
