import axios from "axios";
import url from "./constants.js";

export const RenewAccessToken = () => (
    new Promise((resolve, reject) => {
        console.log("Renew token");
        const options = {
            method: "post",
            url: `${url}/refresh`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
            },
        };
        axios(options).then((res) => {
            if (res.data.token) {
                localStorage.setItem("accessToken", res.data.token);
            } else {
                localStorage.removeItem("accessToken");
            }
            if (res.data.token) resolve();
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const Login = (userID, password) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "post",
            url: `${url}/login`,
            credentials: "include",
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
            data: {
                userID: parseInt(userID, 10),
                password,
            },
        };
        axios(options).then((res) => {
            if (res.data.token) {
                localStorage.setItem("accessToken", res.data.token);
            } else {
                localStorage.removeItem("accessToken");
            }
            if (res.data.token) resolve();
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const Logout = () => (
    new Promise((resolve, reject) => {
        const options = {
            method: "post",
            credentials: "include",
            withCredentials: true,
            url: `${url}/logout`,
        };
        axios(options).then(() => {
            localStorage.removeItem("accessToken");
            resolve();
        }).catch(() => {
            reject();
        });
    })
);
export const SignUp = (name, password, balance) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "post",
            url: `${url}/signup`,
            credentials: "include",
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
            data: {
                name,
                password,
                balance,
            },
        };
        axios(options).then((res) => {
            if (res.data.token) {
                localStorage.setItem("accessToken", res.data.token);
            } else {
                localStorage.removeItem("accessToken");
            }
            if (res.data.token) resolve();
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const GetAccount = () => (
    new Promise((resolve, reject) => {
        const options = {
            method: "get",
            url: `${url}/userID/`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
            },
        };
        axios(options).then((res) => {
            resolve(res.data);
        }).catch(() => {
            reject();
        });
    })
);
export const TransferFunds = (recipientID, amount) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "put",
            url: `${url}/userID/transfer`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type": "application/json",
            },
            data: {
                recipientID,
                amount,
            },
        };
        axios(options).then((res) => {
            if (res.data) resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const DepositFunds = (amount) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "put",
            url: `${url}/userID/deposit`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type": "application/json",
            },
            data: {
                amount,
            },
        };
        axios(options).then((res) => {
            if (res.data) resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const WithdrawFunds = (amount) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "put",
            url: `${url}/userID/withdraw`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type": "application/json",
            },
            data: {
                amount,
            },
        };
        axios(options).then((res) => {
            if (res.data !== undefined) resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const ModifyAccount = (newName, newPassword) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "put",
            url: `${url}/userID/modify`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type": "application/json",
            },
            data: {
                newName,
                newPassword,
            },
        };
        axios(options).then((res) => {
            if (res.data) resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const CreateFundRequest = (issuer, responder, amount) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "post",
            url: `${url}/userID/fundRequest`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type": "application/json",
            },
            data: {
                issuer,
                responder,
                amount,
            },
        };
        axios(options).then((res) => {
            if (res.data) resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const GetAllRequestsFor = () => (
    new Promise((resolve, reject) => {
        const options = {
            method: "get",
            url: `${url}/userID/fundRequest`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
            },
        };
        axios(options).then((res) => {
            console.log(res);
            if (res.data) resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const AcceptRequestWithID = (requestID) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "delete",
            url: `${url}/userID/fundRequest/accept`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type": "application/json",
            },
            data: {
                request_id: requestID,
            },
        };
        console.log(options);
        axios(options).then((res) => {
            if (res.data) resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
export const DeclineRequestWithID = (request_id) => (
    new Promise((resolve, reject) => {
        const options = {
            method: "delete",
            url: `${url}/userID/fundRequest/decline`,
            credentials: "include",
            withCredentials: true,
            headers: {
                authentication: `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type": "application/json",
            },
            data: {
                request_id,
            },
        };
        axios(options).then((res) => {
            if (res.data)resolve(res.data);
            else reject();
        }).catch(() => {
            reject();
        });
    })
);
