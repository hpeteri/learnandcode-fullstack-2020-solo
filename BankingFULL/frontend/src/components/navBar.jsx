import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import "../css/styleSheet.css";
//Sets current view in viewContainer
const NavBar = (props) => {
    const viewComponentBindings = props.views;
    return (
        <div>
          {Object.keys(viewComponentBindings).map((it, index) => {
              return (
                  <Link to={it.replace(" ", "")}>
                    <button key={it} className="navButton" > {it} </button>
                  </Link>
              )
          })}
        </div>
    );    
};
export default NavBar;
